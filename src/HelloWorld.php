<?php

namespace HelloWorld;

class HelloWorld
{
    protected $author;

    /**
     * HelloWorld constructor.
     * @param string $author
     */
    public function __construct($author = 'Bergman')
    {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function info()
    {
        $info = 'Hello composer package!';
        $info .= '\t--Power By:';
        $info .= $this->author . '\n';
        return $info;
    }

}